/// @description Set up camera

//General Variables
cam = view_camera[0];
follow = obj_player;

view_width_half = camera_get_view_width(cam) * 0.5; //Enables the setting of the cameras origin to the middle
view_height_half = camera_get_view_height(cam) * 0.5;

xTo = xstart; //Represent the x and y coordinates we are moving towards
yTo = ystart;

