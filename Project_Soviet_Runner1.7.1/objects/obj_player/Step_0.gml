/// @description Player Logic & Camera


// Get player inputs
if (hascontrol)
{
	key_left  = keyboard_check(vk_left) || keyboard_check(ord("A")) ||(gamepad_axis_value(4, gp_axislh) < 0) || (gamepad_axis_value(0, gp_axislh) < 0);
	key_right = keyboard_check(vk_right) || keyboard_check(ord("D"))|| (gamepad_axis_value(4, gp_axislh) > 0) || (gamepad_axis_value(0, gp_axislh) > 0);
	key_jump  = keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up) || gamepad_button_check_pressed(4,gp_face1) || gamepad_button_check_pressed(0,gp_face1);
	//key_punch = keyboard_check_pressed(ord("D") || gamepad_button_check_pressed(0,gp_face3);
	key_shoot = keyboard_check_pressed(vk_space) || keyboard_check_pressed(vk_enter) || gamepad_button_check_pressed(4, gp_shoulderrb) ||  gamepad_button_check_pressed(0, gp_shoulderrb);
	key_dodge = keyboard_check(ord("S")) || keyboard_check(vk_down) || (gamepad_axis_value(4, gp_axislv) > 0) || (gamepad_axis_value(0, gp_axislv) > 0);
	key_quickRestart = keyboard_check_pressed(ord("R")) || gamepad_button_check_pressed(4,gp_face4) || gamepad_button_check_pressed(0,gp_face4)
}
else
{
	key_jump = 0;
	key_left = 0;
	key_right = 0;
}


// Calculate movement
var _move = key_right - key_left;

hsp = _move * walksp;

vsp = vsp + grv;

// Start timer


// Checking for jump
if (place_meeting(x, y + 3, obj_platform) and (key_jump))
{
	vsp = - jumpsp;
}


// Horizontal Collision
if (place_meeting(x + hsp, y, obj_platform))
{
	while (!place_meeting(x + sign(hsp), y, obj_platform))
	{
		x = x + sign(hsp);
	}
	
	hsp = 0;
}

x = x + hsp;


// Vertical Collision
if (place_meeting(x , y + vsp, obj_platform))
{
	while (!place_meeting(x , y + sign(vsp), obj_platform))
	{
		y = y + sign(vsp); 
	}
	if (place_meeting(x, y+vsp, obj_platform) && (vsp > 25)) 
	{
		audio_play_sound(snd_player_death,12,false);
		
		instance_destroy(obj_player);
		instance_create_layer(256,3936,"Player",obj_player)
	}
	vsp = 0;
}
 
y = y + vsp;


// Animation
if (!place_meeting(x, y + 3, obj_platform))
{
	sprite_index = spr_player_jump;
	image_speed = 0;
	if (sign(vsp) > 0) image_index = 1; //else image_index = 0;
}
else
{
	image_speed = 1;
	if (hsp == 0)
	{
		sprite_index = spr_player_idle;
		
		// Dodge Move
		if (key_dodge)
		{
			sprite_index = spr_player_dodge;
		}
		
	}
	else
	{
		sprite_index = spr_player_walk;
	}
}

// Turning the player sprite
if (hsp != 0) image_xscale = sign(hsp); 
// Sign returns either 1 or -1, depending on if something is true or not. 


// Shooting
if (image_xscale = -1 && key_shoot)
{
	audio_play_sound(snd_player_blaster,10,false);
	with (instance_create_layer(obj_player.x-15, obj_player.y, "Bullets", obj_bullet)) hspeed = -9;
	image_speed = 0.05;
}

if (image_xscale = 1 && key_shoot)
{
	audio_play_sound(snd_player_blaster,10,false);
	with (instance_create_layer(obj_player.x+15, obj_player.y, "Bullets", obj_bullet)) hspeed = 9;
	image_speed = 0.05;
}

// Check for quick restart
if (key_quickRestart)
{
	room_restart();
}