/// @description Various screens

switch(room)
{
	case rm_menu:
	draw_set_halign(fa_center);
	var c = c_red;
	var d = c_yellow;
		draw_text_transformed_color(room_width/2, 253, 
		"SOVIET GHOSTRUNNER", 2, 2, 0, c,c,c,c, 1);
		
		draw_text_transformed_color(room_width/2, 500, 
		@"Privet comrade!
Try to exterminate all hostiles in the area as fast as possible by jumping and shooting.
The clock will start the moment you are deployed. Uspekhov!

Navigate trough the menu on keyboard with arrow keys and enter 
or on gamepad with the left stick and 'O'/'B' button (Playstation or Xbox).
To go back, press ESC (Keyboard) or START (Gamepad).
", 0.7, 0.7, 0, d,d,d,d, 1);
		
		draw_set_halign(fa_left);
	break;
	
	case rm_transition1:
	draw_set_halign(fa_center);
	var c = c_red;
	var d = c_yellow;
		draw_text_transformed_color(room_width/2, 253, 
		"You won!", 2, 2, 0, c,c,c,c, 1);
		
		draw_text_transformed_color(room_width/2, room_height/2, 
		"You did it comrade! Your time was: ", 1, 1, 0, d,d,d,d, 1);
		
		if ((global.seconds < 10) && (global.minutes <10))
	{
		draw_text_transformed_color(room_width/2, 500, "0" + string(global.minutes) + ":0" + string(global.seconds), 2, 2, 0,c,c,c,c, 1);
	}
	
	else if ((global.seconds >= 10) && (global.minutes >= 10))
	{
		draw_text_transformed_color(room_width/2, 500, string(global.minutes) + ":" + string(global.seconds), 2, 2, 0,c,c,c,c,1);
	}
	
	else if ((global.seconds < 10) && (global.minutes >= 10))
	{
		draw_text_transformed_color(room_width/2, 500, string(global.minutes) + ":0" + string(global.seconds), 2, 2, 0,c,c,c,c,1);
	}
	
	else if ((global.seconds >= 10) && (global.minutes < 10))
	{
		draw_text_transformed_color(room_width/2, 500, "0" + string(global.minutes) + ":" + string(global.seconds), 2, 2, 0,c,c,c,c,1);
	}
	/*draw_text_transformed_color(room_width/2, 500, 
		string(global.minutes) + ":" + string(global.seconds), 1, 1, 0, d,d,d,d, 1);*/
		
		draw_text_transformed_color(room_width/2, 650, 
		"Press ENTER or 'O'/'B'  on your gamepad", 1, 1, 0, d,d,d,d, 1);
		draw_set_halign(fa_left);
	break;
	
	case rm_controls:
	draw_set_halign(fa_center);
	var c = c_red;
	var d = c_yellow;
	
	draw_text_transformed_color(room_width/2, 253, 
		"Controls", 2, 2, 0, c,c,c,c, 1);
		
		draw_text_transformed_color(room_width/2, 650, 
		@"Keyboard:
Arrow Keys / 'A' and 'D' Keys = Left/Right Movement
Space / Enter Keys = Shooting
'W' / 'Up' Keys = Jump
'S' / 'Down' Keys = Dodge
ESC Key = Return to Menu
'R' Key = Quick Restart

Gamepad:
Left Stick horizontal = Left/Right Movement
'X' / 'A' = Jumping (on Playstation or Xbox Gamepad)
Left Stick Down = Dodge
Right Shoulder Trigger = Shooting
Start Button = Return to Menu
'Triangle' / 'Y' Button = Quick Restart", 0.7, 0.7, 0, d,d,d,d, 1);
		draw_set_halign(fa_left);
	break;
	

	
	case rm_credits:
	draw_set_halign(fa_center);
	var c = c_red;
	var d = c_yellow;
		draw_text_transformed_color(room_width/2, 253, 
		"Credits", 2, 2, 0, c,c,c,c, 1);
		
		draw_text_transformed_color(room_width/2, 500, 
		@"Art, Leveldesign, Character Animation and Idea by Bent Sinnecker
Coding by Lauritz Sammann
Enemy Sprite found on opengameart.org 
Chopper Design by Morgane Thieurmel on arstation.com
Tank Design by SL4Y3R28 on woodpress.com

Sounds found on freesound.org
Soundtrack by Patrick de Arteaga
One enemy scream by theuncertainman", 0.7, 0.7, 0, d,d,d,d, 1);
		
		draw_set_halign(fa_left);
	break;
	
	case rm_story:
	draw_set_halign(fa_center);
	draw_set_font(f_menu);
	var c = c_red;
	var d = c_yellow;
		draw_text_transformed_color(room_width/2, 50, 
		"Soviet Runner", 2, 2, 0, c,c,c,c, 1);
		
		draw_text_transformed_color(room_width/2, 200, 
		@"The year is 2065. By means of a breakthrough method, the spirit
of former Soviet Union founder Vladimir Lenin has been successfully digitized. 
What followed was a miracle - Digital-Lenin quickly established himself as the
voice of the people and even managed to reunite the former Soviet states. 
The new CCCP was born, this time without tyranny. But something is brewing. Rumor
has it that a digital version of Josef Stalin is now assembling an underground army
in order to regain power himself. By any means necessary, it seems. The population 
is afraid and needs a hero - you. You're on a mission as a Soviet Ghostrunner to 
find these renegades and take them out as quickly as possible. 
Think you have what it takes? Then grab a bottle of vodka, put on your
best tracksuit, neutralize these traitors and beat the clock!", 0.7, 0.7, 0, d,d,d,d, 1);
		draw_text_transformed_color(room_width/2, y, 
		"Press ENTER or 'O'/'B'  on your gamepad", 0.7, 0.7, 0, d,d,d,d, 1);
		draw_set_halign(fa_left);
	break;
	
	
}	