/// @description General variables

// Size variables and mode setup

w = display_get_gui_width();
h = display_get_gui_height();
h_half = h * 0.5;

enum TRANS_MODE
{
	OFF,
	NEXT,
	GOTO,
	RESTART,
	INTRO
}

mode = TRANS_MODE.INTRO;
percent = 1;
percenttarget = 1.2; //Wie lange die Transition gehen soll
target = room;


// Quit game
key_quit = vk_escape;
key_gamepadQuit = gp_start;