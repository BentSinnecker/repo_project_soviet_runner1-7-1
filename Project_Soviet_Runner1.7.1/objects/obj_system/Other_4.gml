/// @desc Audio manager

if (room == rm_cave2)
{
	if(audio_is_playing(snd_soundtrack))
	{
		audio_stop_sound(snd_soundtrack);
	}
	audio_play_sound(snd_soundtrack,10,true); 
}

if (room == rm_ice)
{
	if(audio_is_playing(snd_soundtrack))
	{
		audio_stop_sound(snd_soundtrack);
	}
	audio_play_sound(snd_soundtrack,10,true); 
}

if (room == rm_menu)
{
	audio_stop_all(); 
}

if (room == rm_transition1)
{
	if(audio_is_playing(snd_crowdCheer))
	{
		audio_stop_sound(snd_crowdCheer);
	}
	if(audio_is_playing(snd_fanfare))
	{
		audio_stop_sound(snd_fanfare);
	}
	audio_stop_sound(snd_soundtrack); 
	audio_play_sound(snd_crowdCheer,10,false);
	audio_play_sound(snd_fanfare,11,false);
}