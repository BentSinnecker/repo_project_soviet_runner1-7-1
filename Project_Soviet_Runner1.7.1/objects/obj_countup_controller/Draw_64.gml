/// @desc Draw timer


	draw_set_halign(fa_left);
	var c = c_white;
	
	if ((seconds < 10) && (minutes <10))
	{
		draw_text_color(50, 50, "Your time: 0" + string(minutes) + ":0" + string(seconds),c,c,c,c,1);
	}
	
	else if ((seconds >= 10) && (minutes >= 10))
	{
		draw_text_color(50, 50, "Your time: " + string(minutes) + ":" + string(seconds),c,c,c,c,1);
	}
	
	else if ((seconds < 10) && (minutes >= 10))
	{
		draw_text_color(50, 50, "Your time: " + string(minutes) + ":0" + string(seconds),c,c,c,c,1);
	}
	
	else if ((seconds >= 10) && (minutes < 10))
	{
		draw_text_color(50, 50, "Your time: 0" + string(minutes) + ":" + string(seconds),c,c,c,c,1);
	}
