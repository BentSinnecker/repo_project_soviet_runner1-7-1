/// @description Movement, grv, shooting
vsp = vsp + grv;


//Horizontal Collision
if (place_meeting(x + hsp, y, obj_wall))
{
	while (!place_meeting(x + sign(hsp), y, obj_wall))
	{
		x = x + sign(hsp);
	}
	
	hsp = -hsp;
	
}

x = x + hsp;


//Vertical Collision
if (place_meeting(x , y + vsp, obj_platform))
{
	while (!place_meeting(x , y + sign(vsp), obj_platform))
	{
		y = y + sign(vsp);
	}
	
	vsp = 0;
}
 
y = y + vsp;


// Animation
if (place_meeting(x, y + 1, obj_platform))
{
	
	sprite_index = spr_sovietflamemaster_f1; 
	image_speed = 1;
	//if (sign(vsp) > 0) image_index = 1; else image_index = 0;
}
else
{
	
	image_speed = 1;
	if (hsp == 0)
	{
		sprite_index = spr_sovietflamemaster_f1; //Idle
	}
	else
	{
		sprite_index = spr_sovietflamemaster_f1;
	}
}



// Turning the player sprite
if (hsp != 0) image_xscale = sign(hsp); 



//Detecting and shooting
var _dir = sign(obj_player.x - x) //if player is on right, this is 1, -1 if player is on left
if (point_distance(obj_player.x, obj_player.y, x, y) < 600 && image_xscale == _dir)
{
    countdown--;
    if (countdown <= 0)
    {
        countdown = countdownrate;
        if (obj_player.x < x)
        {
            with (instance_create_layer(x-15, y-3, "Bullets", obj_E_bullet))
            {
				audio_play_sound(snd_enemy_blaster,12,false);
                hspeed = -9;
                image_speed = 0.05;
            }
        }
        else
        {
            with (instance_create_layer(x+15, y-3, "Bullets", obj_E_bullet))
            {
				audio_play_sound(snd_enemy_blaster,12,false);
                hspeed = 9;
                image_speed = 0.05;
            }
        }
    }
}


