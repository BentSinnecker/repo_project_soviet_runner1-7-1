/// @desc Control menu

// Item ease in
menu_x += (menu_x_target - menu_x) / menu_speed;

// Menu Controls
if (menu_control)
{
	if (keyboard_check_pressed(vk_up) || (gamepad_button_check_pressed(4, gp_padu)) || (gamepad_button_check_pressed(0, gp_padu)))
	{
		menu_cursor++;
		if (menu_cursor >= menu_items) menu_cursor = 0;
	}
	
	if (keyboard_check_pressed(vk_down) || (gamepad_button_check_pressed(4, gp_padd)) || (gamepad_button_check_pressed(0, gp_padd)))
	{
		menu_cursor--;
		if (menu_cursor < 0) menu_cursor = menu_items-1;
	}
	
	if (keyboard_check_pressed(vk_enter) || (gamepad_button_check_pressed(4, gp_face2)) || (gamepad_button_check_pressed(0, gp_face2)))
	{
		menu_x_target = gui_width + 200;
		menu_committed = menu_cursor;
		menu_control = false;
	}
}

// Transition manager
if (menu_x > gui_width + 150) && (menu_committed != -1)
{
	switch(menu_committed)
	{
		case 4: SlideTransition(TRANS_MODE.GOTO, rm_cave2); break;
		case 3: default: SlideTransition(TRANS_MODE.GOTO, rm_ice); break;
		case 2: SlideTransition(TRANS_MODE.GOTO, rm_controls); break;
		case 1: SlideTransition(TRANS_MODE.GOTO, rm_credits); break;
		case 0: game_end(); break;
	}
}
