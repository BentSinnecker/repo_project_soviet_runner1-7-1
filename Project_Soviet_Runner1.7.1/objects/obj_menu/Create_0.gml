/// @desc GUI/Vars/Menu setup


gui_width = display_get_gui_width();
gui_height = display_get_gui_height();
gui_margin = 32;

menu_x = gui_width + 200;
menu_y = gui_height - gui_margin;
menu_x_target = gui_width - gui_margin;
menu_speed = 25 // Lower is faster
menu_itemheight = font_get_size(f_menu);
menu_committed = -1;
menu_control = true;

menu[4] = "Cave Level";
menu[3] = "Ice Level";
menu[2] = "Controls";
menu[1] = "Credits";
menu[0] = "Quit";

menu_items = array_length(menu);
menu_cursor = 4;