
instance_destroy();

with(other)
{
	audio_play_sound(choose(snd_enemy_death, snd_enemy_death2, snd_enemy_death3, snd_enemy_death4, snd_enemy_death5),7,false);
	instance_destroy();
}

repeat(20)
{
	instance_create_layer(x,y, "Enemies", obj_blood);
}