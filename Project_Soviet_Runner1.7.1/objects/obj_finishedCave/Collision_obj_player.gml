/// @desc

// Pull time from stopwatch
if (obj_countup_controller.count_up == true)
{
	obj_countup_controller.count_up = false;
	global.seconds = obj_countup_controller.seconds;
	global.minutes = obj_countup_controller.minutes;
}

// Transition
with (obj_player)
{
	if (hascontrol)
	{
		hascontrol = false;
		SlideTransition(TRANS_MODE.GOTO, other.target); 
		//TARGET HAS TO BE DEFINED IN THE CREATION CODE OF EACH TRIGGER
	}
}