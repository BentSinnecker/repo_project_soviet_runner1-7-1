{
  "compression": 0,
  "volume": 0.27,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_crowdCheer.wav",
  "duration": 6.97866726,
  "parent": {
    "name": "General",
    "path": "folders/Sounds/General.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_crowdCheer",
  "tags": [],
  "resourceType": "GMSound",
}