{
  "compression": 0,
  "volume": 0.24,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_enemy_death3.wav",
  "duration": 1.428628,
  "parent": {
    "name": "Enemy",
    "path": "folders/Sounds/Enemy.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_enemy_death3",
  "tags": [],
  "resourceType": "GMSound",
}