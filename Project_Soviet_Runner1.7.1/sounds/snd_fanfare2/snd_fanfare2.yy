{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_fanfare2.wav",
  "duration": 1.72175694,
  "parent": {
    "name": "General",
    "path": "folders/Sounds/General.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_fanfare2",
  "tags": [],
  "resourceType": "GMSound",
}