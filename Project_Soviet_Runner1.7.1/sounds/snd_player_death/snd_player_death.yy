{
  "compression": 0,
  "volume": 0.33,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_player_death.wav",
  "duration": 1.038288,
  "parent": {
    "name": "Player",
    "path": "folders/Sounds/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_player_death",
  "tags": [],
  "resourceType": "GMSound",
}