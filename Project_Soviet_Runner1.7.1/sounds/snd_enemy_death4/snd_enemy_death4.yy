{
  "compression": 0,
  "volume": 0.34,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_enemy_death4.wav",
  "duration": 1.121848,
  "parent": {
    "name": "Enemy",
    "path": "folders/Sounds/Enemy.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_enemy_death4",
  "tags": [],
  "resourceType": "GMSound",
}