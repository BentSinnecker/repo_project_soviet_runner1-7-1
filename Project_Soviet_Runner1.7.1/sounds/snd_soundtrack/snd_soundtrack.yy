{
  "compression": 1,
  "volume": 0.14,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_soundtrack.ogg",
  "duration": 86.90155,
  "parent": {
    "name": "General",
    "path": "folders/Sounds/General.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_soundtrack",
  "tags": [],
  "resourceType": "GMSound",
}