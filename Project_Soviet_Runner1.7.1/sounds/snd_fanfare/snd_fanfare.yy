{
  "compression": 0,
  "volume": 0.43,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_fanfare.wav",
  "duration": 4.442483,
  "parent": {
    "name": "General",
    "path": "folders/Sounds/General.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_fanfare",
  "tags": [],
  "resourceType": "GMSound",
}