{
  "compression": 0,
  "volume": 0.47,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_player_jetpack.wav",
  "duration": 1.99966693,
  "parent": {
    "name": "Player",
    "path": "folders/Sounds/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_player_jetpack",
  "tags": [],
  "resourceType": "GMSound",
}